const key = process.env.API_KEY;

import { google } from 'googleapis';
import { youtube_v3 } from 'googleapis/build/src/apis/youtube/v3';
const yt = google.youtube('v3');

export async function doQuery(id: string): Promise<youtube_v3.Schema$VideoSnippet> {
    const ret = await yt.videos.list({
        key, id, part: 'snippet',
    });

    const d = ret.data.items;
    if(d === undefined) throw new Error("Unable to find video");

    for (const vid of d) {
        if(vid.snippet === undefined) continue;
        if(vid.snippet.title === undefined) continue;

        return vid.snippet;
    }

    throw new Error("Id not found");
}

export async function searchVideo(search: string) {
    const ret = await yt.search.list({
         key,
         q: search,
         type: 'video',
         maxResults: 19,
         part: 'snippet',
    });

    const d = ret.data.items;
    if(d === undefined) throw new Error("Unable to find video");

    return d;
}

