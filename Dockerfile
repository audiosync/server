FROM node:alpine as build-stage

WORKDIR /app

RUN npm install typescript -g

# Install package as a separate layer for improved docker caching
COPY package*.json ./
RUN npm install

# Build the application
COPY . .
RUN tsc -p tsconfig.json 
CMD ["node", "./build/main.js"]
EXPOSE 80
