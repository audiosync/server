import socketIO = require('socket.io');

import { doQuery, searchVideo } from './auth';

const io = socketIO(80, { pingInterval: 2000 });

io.on('connection', socket => {
  socket.on('search', async (search: string) => {
    socket.emit('search', await searchVideo(search));
  });
  socket.on('setVideo', async (videoUrl: string) => {
    const { title, channelTitle, thumbnails } = await doQuery(videoUrl);
    let thumbnail: string | undefined;
    if(thumbnails) {
      const medThumbnail = thumbnails.medium;
      if(medThumbnail) {
        thumbnail = medThumbnail.url;
      }
    }

    io.emit('setVideo', {
      videoUrl, title, channelTitle,
      thumbnail: thumbnails!.medium!.url
    });
  });

  socket.on('setTime', time => {
    io.emit('setTime', time);
  });
});

/*
const room = io.sockets.adapter.rooms;
for(const r of room) {
	console.log(room)
}
*/
